import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
          <div id="wrapper">
              <div id="content">
                  Поле сообщений
              </div>
              <div id="contacts">
                  Поле контактов
              </div>
          </div>
      </div>
    );
  }
}

export default App;
